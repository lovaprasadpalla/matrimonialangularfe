import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.css']
})
export class ViewprofileComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  imgCollection: Array<object> = [
    {
      image: "../../assets/m1.jpg",
      thumbImage: "../../assets/m1.jpg",
      alt: 'Image 1',
      title: 'Image 1'
    }, {
      image: "../../assets/m1.jpg",
      thumbImage: "../../assets/m1.jpg",
      title: 'Image 2',
      alt: 'Image 2'
    }, {
      image: "../../assets/m1.jpg",
      thumbImage: "../../assets/m1.jpg",
      title: 'Image 3',
      alt: 'Image 3'
    }, {
      image: "../../assets/m1.jpg",
      thumbImage: "../../assets/m1.jpg",
      title: 'Image 4',
      alt: 'Image 4'
    }, 
];
}
