import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import {  RegisterComponent } from './register/register.component';
import { IndexComponent } from './index/index.component';
import { Register1Component } from './register1/register1.component';
import { AboutComponent } from './about/about.component';
import { PresidentComponent } from './president/president.component';
import { ServiceComponent } from './service/service.component';
import { ContactComponent } from './contact/contact.component';
import { HeaderComponent } from './header/header.component';
import { PraticeComponent } from './pratice/pratice.component';
import { Header1Component } from './header1/header1.component';
import { HomeComponent } from './home/home.component'; 
import { NewmatchesComponent } from './newmatches/newmatches.component';
import { MyprofileComponent } from './myprofile/myprofile.component';
import { ViewprofileComponent } from './viewprofile/viewprofile.component';
const routes: Routes = [
  {path:'login',component:LoginComponent},
  { path:'register',component:RegisterComponent},
  { path:'index',component:IndexComponent},
  { path:'',component:IndexComponent},
  { path:'register1',component:Register1Component},
  { path:'about',component:AboutComponent},
  { path:'president',component:PresidentComponent},
  { path:'service',component:ServiceComponent},
  { path:'contact',component:ContactComponent},
  { path:'header',component:HeaderComponent},
  {path:'pratice',component:PraticeComponent},
  { path:'header1',component:Header1Component},
  { path:'home',component:HomeComponent},
  { path:'newmatches',component:NewmatchesComponent},
  { path:'myprofile',component:MyprofileComponent},
  { path:'viewprofile',component:ViewprofileComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
