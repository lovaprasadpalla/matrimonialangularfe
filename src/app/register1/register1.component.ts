import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';


@Component({
  selector: 'app-register1',
  templateUrl: './register1.component.html',
  styleUrls: ['./register1.component.css'],
  
})
export class Register1Component implements OnInit {
  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });
  
  isLinear = false
  customer:any = [{pname:"5days Freetrail with subscription "},{pname:"cancel"}]
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    
  }
  changeClient(value:any) {
    console.log(value);
}

}